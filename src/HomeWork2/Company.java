package HomeWork2;

/*司有公司名称，注册地址，电话，经营范围等属性，公司有很多员工（暂用数组表示），公司有招聘员工方法，有输出员工信息方法
 * ，以及辞退员工方法(*,想不明白可以先不管)。用oo的思想模拟*/
public class Company {
	private String name = "soft", address = "heifei", work = "programming";
	private String[] worker = new String[30];

	/**
	 * 获得姓名
	 */
	public String getName() {
		return name;
	}

	/**
	 * 获得地址
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * 获得地址
	 */
	public String getWork() {
		return work;
	}

	/**
	 * 添加员工信息
	 * 
	 * @param name
	 * @return
	 */
	public void add(String name) {
		for (int i = 0; i < worker.length; i++) {
			if (worker[i] == null) {
				worker[i] = name;
				return;
			}
		}
	}

	/**
	 * 输出员工信息
	 */
	public void show() {
		for (int i = 0; i < worker.length; i++) {
			if (worker[i] != null) {
				System.out.print(worker[i] + " ");
				if (i % 5 == 0 && i != 0)
					System.out.println();
				continue;
			}
			return;
		}
	}

	/**
	 * 辞退员工方法
	 * 
	 * @param name
	 */
	public void delete(String name) {
		for (int i = 0; i < worker.length; i++) {
			if (worker[i].equals(name)) {
				worker[i] = null;
				for (int j = i; j < worker.length - 1; j++) {
					worker[j] = worker[j + 1];
				}
				return;
			}
		}
	}
}
