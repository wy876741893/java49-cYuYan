package HomeWork2;

import java.util.Scanner;

/*/*司有公司名称，注册地址，电话，经营范围等属性，公司有很多员工（暂用数组表示），公司有招聘员工方法，有输出员工信息方法
 * ，以及辞退员工方法(*,想不明白可以先不管)。用oo的思想模拟*/
public class TestCompany {

	public static void main(String[] args) {
		Company c = new Company();
		Scanner sc = new Scanner(System.in);
		System.out.println("公司名：" + c.getName() + " 注册地址" + c.getAddress() + " 经营范围" + c.getWork());
		while (true) {
			System.out.println("1.招聘员工\t2.输出员工信息\t3.辞退员工\t4.退出");
			switch (sc.nextInt()) {
			case 1:
				System.out.println("输入员工姓名：");
				c.add(sc.next());
				break;
			case 2:
				c.show();
				break;
			case 3:
				System.out.println("输入要删除的员工姓名：");
				c.delete(sc.next());
				break;
			case 4:
				return;
			default:
				System.out.println("非法输入");
			}
		}

	}

}
