package HomeWork5;

/*回顾：验证哥德巴赫猜想：任何一个大于6的偶数，都能分解成两个质数和。
 要求输入一个数，如果不满足条件，则重新输入；

然后分解成两个质数相加的格式，类似  10=3+7这样
 * */
public class NumberPlus {
	/**
	 * 判断是否是质数
	 */
	public boolean choice(int num) {
		for (int i = 2; i < num; i++) {
			if (num % 2 == 0)
				return false;
		}
		if (num < 2 && num >= 0)
			return false;
		return true;
	}

	/**
	 * 分解质数
	 */
	public int get(int i) {
		for (int n = 2; n < i / 2; n++) {
			if (choice(n)) {
				if (choice(i - n)) {
					return n;
				}
			} else
				return 0;
		}
		return 0;
	}

}
