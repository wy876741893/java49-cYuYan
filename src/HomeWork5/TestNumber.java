package HomeWork5;

import java.util.Scanner;

/*回顾：验证哥德巴赫猜想：任何一个大于6的偶数，都能分解成两个质数和。
要求输入一个数，如果不满足条件，则重新输入；

然后分解成两个质数相加的格式，类似  10=3+7这样*/
public class TestNumber {

	public static void main(String[] args) {
		NumberPlus s = new NumberPlus();
		System.out.println("输入大于6的偶数");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println(num + "=" + s.get(num) + "+" + (num - s.get(num)));

	}

}
