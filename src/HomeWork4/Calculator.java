package HomeWork4;

import java.util.Scanner;

//计算器用颜色，品牌，价格等属性，有计算加减乘除以及算立方的方法，用oo的思想模拟。
public class Calculator {
	private String color = "black", brand = "卡西欧", price = "￥80", expression;
	Scanner sc = new Scanner(System.in);

	/**
	 * 返回颜色
	 */
	public String getColor() {
		return color;
	}

	/**
	 * 返回品牌
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * 返回价格
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * 计算加减乘除
	 */
	public String comput(String s) {
		if (s.indexOf("+") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("+")));
			double b = Double.parseDouble(s.substring(s.indexOf("+") + 1, s.length()));
			return s + "=" + (a + b);
		} else if (s.indexOf("-") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("-")));
			double b = Double.parseDouble(s.substring(s.indexOf("-") + 1, s.length()));
			return s + "=" + (a - b);
		} else if (s.indexOf("*") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("*")));
			double b = Double.parseDouble(s.substring(s.indexOf("*") + 1, s.length()));
			return s + "=" + (a * b);
		} else if (s.indexOf("/") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("/")));
			double b = Double.parseDouble(s.substring(s.indexOf("/") + 1, s.length()));
			return s + "=" + (a / b);
		} else
			return s;
	}

	/**
	 * 计算立方
	 */
	public String computSum(String s) {
		double sum = Double.parseDouble(s) * Double.parseDouble(s) * Double.parseDouble(s);
		return "" + sum;
	}

	/**
	 * 加减乘除返回计算后值
	 */
	public String computPlus(String s) {
		if (s.indexOf("+") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("+")));
			double b = Double.parseDouble(s.substring(s.indexOf("+") + 1, s.length()));
			return "" + (a + b);
		} else if (s.indexOf("-") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("-")));
			double b = Double.parseDouble(s.substring(s.indexOf("-") + 1, s.length()));
			return "" + (a - b);
		} else if (s.indexOf("*") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("*")));
			double b = Double.parseDouble(s.substring(s.indexOf("*") + 1, s.length()));
			return "" + (a * b);
		} else if (s.indexOf("/") >= 0) {
			double a = Double.parseDouble(s.substring(0, s.indexOf("/")));
			double b = Double.parseDouble(s.substring(s.indexOf("/") + 1, s.length()));
			return "" + (a / b);
		} else
			return s;
	}
}
