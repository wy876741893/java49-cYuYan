package HomeWork4;

import java.util.Scanner;

public class TestCalculator {

	public static void main(String[] args) {
		Calculator c = new Calculator();
		System.out.println("计算器颜色" + c.getColor() + "，品牌" + c.getBrand() + "，价格" + c.getPrice());
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("1.计算加减乘除\t2.计算立方\t3.退出");
			int n = sc.nextInt();
			if (n == 3)
				return;
			System.out.println("输入表达式 eg:（a+b）:");
			String expression = sc.next();
			switch (n) {
			case 1:
				System.out.println(c.comput(expression));
				break;
			case 2:
				System.out.println("s的立方为" + c.computSum(c.computPlus(expression)));
				break;
			default:
				System.out.println("非法输入");
			}
		}

	}

}
