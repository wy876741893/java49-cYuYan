package HomeWork1;

/*
 * 在我的案例的基础上，添加查找指定位置的客户姓名的方法，方法的声明如下：
  public String find(int index)
,并提供完整测试。*/
public class InsureManager {
	String name, tele;
	String no;
	String customers[] = new String[30];

	/**
	 * 增加新客户 找到一个空的位置，把新客户放进去
	 * 
	 * @param name
	 */
	public boolean add(String name) {
		for (int i = 0; i < customers.length; i++) {
			if (customers[i] == null) {
				customers[i] = name;
				return true;
			}
		}
		return false;
	}

	public void showInfo() {
		System.out.println("这个业务员叫" + name + ",手机号" + tele + ",工号" + no);
		System.out.println("管理的客户有：");
		for (int i = 0; i < customers.length; i++) {
			if (customers[i] == null)
				continue;// return

			System.out.print(customers[i] + "\t");
			if (i % 5 == 0 && i != 0)
				System.out.println();
		}
		System.out.println();
	}

	/**
	 * 根据客户名称查找客户，返回false,表示没有找到；true表示找到了
	 * 
	 * @param name
	 * @return
	 */
	public boolean search(String name) {
		for (int i = 0; i < customers.length; i++) {
			if (customers[i] != null && customers[i].equals(name)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * 限制范围查找，需要考虑范围是否合理
	 * 
	 * @param start
	 * @param end
	 * @param name
	 * @return
	 */
	public boolean search2(int start, int end, String name) {
		for (int i = start; start >= 0 && end < customers.length && i < end; i++) {
			if (customers[i] != null && customers[i].equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 修改客户名称：通过老名字找到客户，修改成新名字，如果找不到，返回false
	 * 
	 * @param oldName
	 * @param newName
	 * @return
	 */
	public boolean modifyName(String oldName, String newName) {
		for (int i = 0; i < customers.length; i++) {
			if (oldName.equals(customers[i])) {
				customers[i] = newName;
				return true;
			}
		}
		return false;
	}

	/**
	 * 查找指定位置的客户
	 * 
	 * @param index
	 * @return
	 */
	public String find(int index) {
		if (index < customers.length && customers[index] != null)
			return "位置" + index + "客户为" + customers[index];
		return "无次人";
	}
}
