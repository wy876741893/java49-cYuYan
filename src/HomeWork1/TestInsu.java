package HomeWork1;

/*
 * *
 * 在我的案例的基础上，添加查找指定位置的客户姓名的方法，方法的声明如下：
  public String find(int index)
,并提供完整测试。*/
import java.util.Scanner;

public class TestInsu {

	public static void main(String[] args) {
		InsureManager m = new InsureManager();
		m.name = "赵杨洋";
		m.no = "9999";
		m.tele = "138888888888";
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		while (true) {
			System.out.println("1:增加新客户，2:显示老客户,3:查找客户，4：修改客户，5:查找指定位置客户，9：退出！");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				System.out.print("请输入新客户的名字：");
				String name = sc.next();
				m.add(name);// 返回值不要了
				break;
			case 2:
				m.showInfo();
				break;

			case 3:
				System.out.println("请输入要查找的名字：");
				String sname = sc.next();
				boolean f = m.search(sname);
				if (f) {
					System.out.println(sname + "是我们的客户");
				} else {
					System.out.println(sname + "不是我们的客户");
				}
				break;
			case 4:
				System.out.println("请输入老名字：");
				String oldName = sc.next();
				System.out.println("请输入新名字：");
				String newn = sc.next();
				boolean ok = m.modifyName(oldName, newn);
				if (ok)
					System.out.println("修改成功");
				else
					System.out.println("修改失败！");
				break;
			case 5:
				System.out.println("输入要查找的位置");
				int index = sc.nextInt();
				System.out.println(m.find(index));
			case 9:
				return;
			}
		}
	}

}
