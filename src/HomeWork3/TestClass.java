package HomeWork3;

import java.util.Scanner;

/*学士后班级有班级名称，班主任姓名，开班日期等属性，
学生信息用数组表示。
用方法模拟 新生加入班级，以及获得班级人数的方法。*/
public class TestClass {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Class s = new Class();
		System.out.println("班级名" + s.getName() + " 班主任名" + s.getClassMater() + " 开班日期" + s.getDate());
		while (true) {
			System.out.println("1.加入新生入班级\t2.或取班级人数\t3.退出");
			switch (sc.nextInt()) {
			case 1:
				System.out.println("输入新生姓名");
				s.add(sc.next());
				break;
			case 2:
				System.out.println("班级人数为" + s.print());
				break;
			case 3:
				return;
			default:
				System.out.println("非法输入");
			}
		}

	}

}
