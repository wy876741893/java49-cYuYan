package HomeWork3;

/*学士后班级有班级名称，班主任姓名，开班日期等属性，
学生信息用数组表示。
用方法模拟 新生加入班级，以及获得班级人数的方法。*/
public class Class {
	private String className = "java", classMater = "x", date = "xxxx.xx.xx";
	private int num;
	private String[] Student = new String[30];

	/**
	 * 获得班级名
	 */
	public String getName() {
		return className;
	}

	/**
	 * 获得班主任姓名
	 */
	public String getClassMater() {
		return classMater;
	}

	/**
	 * 获得建班日期
	 */
	public String getDate() {
		return date;
	}

	/**
	 * 新生加入班级
	 * 
	 * @param student
	 */
	public void add(String student) {
		for (int i = 0; i < Student.length; i++) {
			if (Student[i] == null) {
				Student[i] = student;
				return;
			}
		}
	}

	/**
	 * 输出班级人数总和
	 */
	public int print() {
		int count = 0;
		for (int i = 0; i < Student.length; i++) {
			if (Student[i] != null) {
				count++;
			} else
				return count;
		}
		return count;
	}

}
